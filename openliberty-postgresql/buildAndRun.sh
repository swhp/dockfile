#!/bin/bash
#

set -euo pipefail

docker build -t swardana/openliberty-postgresql .

docker stop openliberty-postgresql || true

docker run --rm -d \
  -p 9080:9080 \
  -p 9443:9443 \
  --name openliberty-postgresql \
  swardana/openliberty-postgresql
